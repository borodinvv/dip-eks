
resource "aws_security_group" "workers-sg-1" {
  name_prefix = "worker-sg-1"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.10.0.0/16",
    ]
  }
}

resource "aws_security_group" "workers-sg-2" {
  name_prefix = "worker-sg-2"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "192.168.0.0/16",
    ]
  }
}

resource "aws_security_group" "workers-sg-all" {
  name_prefix = "worker-sg-all"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.10.0.0/16",
      "172.16.0.0/12",
      "192.168.0.0/16",
    ]
  }
}
